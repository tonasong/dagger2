package com.example.dagger2.MDagger;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;

import dagger.android.support.AndroidSupportInjectionModule;
import dagger.android.support.DaggerApplication;

@Component(modules = {AndroidSupportInjectionModule.class,MyModule.class,ActivityBuilderModule.class})
@Singleton
public interface AppComponent extends AndroidInjector<DaggerApplication> {

    @Component.Builder
    interface Builder{
        @BindsInstance
        Builder applicationa(Application application);

        AppComponent builds();
    }

}
